FROM python:3.11-alpine

WORKDIR /code

COPY . .
RUN pip install .

WORKDIR /data

ENTRYPOINT [ "drope", "--host", "0.0.0.0" ]
